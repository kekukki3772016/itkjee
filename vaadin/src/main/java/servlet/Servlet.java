package servlet;

import gui.VaadinExample;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

@WebServlet(value = "/*", asyncSupported = true)
@VaadinServletConfiguration(productionMode = false,
                            ui = VaadinExample.class)
public class Servlet extends VaadinServlet {
    private static final long serialVersionUID = 1L;
}