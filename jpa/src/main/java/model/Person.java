package model;

import java.util.List;

import javax.persistence.*;

@Entity
@NamedQueries({
    @NamedQuery(name=Person.FIND_ALL,
               query="SELECT p FROM Person p"),
    @NamedQuery(name=Person.FIND_BY_NAME,
               query="SELECT p FROM Person p WHERE p.name = :name"),
})
public class Person extends AbstractEntity {

    public static final String FIND_ALL = "Person.findAll";

    public static final String FIND_BY_NAME = "Person.findByName";

    private String name;

    @Transient
    private int age;

    @OneToMany(cascade = CascadeType.ALL,
               fetch = FetchType.EAGER,
               orphanRemoval = true)
    @JoinColumn(name="person_id", nullable = false)
    private List<Address> addresses;

    @ManyToOne
    private PersonGroup group;

    public Person(Long id, String name) {
        setId(id);
        this.name = name;
    }

    public Person(String name) {
        this.name = name;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person: " + name;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public PersonGroup getGroup() {
        return group;
    }

    public void setGroup(PersonGroup group) {
        this.group = group;
    }
}
