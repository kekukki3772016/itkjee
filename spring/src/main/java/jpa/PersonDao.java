package jpa;

import java.util.List;

import javax.persistence.*;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PersonDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Person person) {
        if (person.getId() == null) {
            em.persist(person);
        } else {
            em.merge(person);
        }
    }

    @Transactional(readOnly = true)
    public List<Person> findAllPersons() {
        return em.createQuery(
                "select p from Person p",
                Person.class).getResultList();
    }
}
