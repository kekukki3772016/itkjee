package aop;

import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

    public void addCustomer(String name){
        System.out.println("\n  addCustomer() is running, args : " + name + "\n");
    }
}