package jdbc;

import javax.sql.DataSource;

import org.springframework.context.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class Main {

    public static void main(String[] args) throws Exception {

        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(Config.class);

        DataSource dataSource = ctx.getBean("hsqlDataSource", DataSource.class);
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        jdbc.execute("DROP SCHEMA PUBLIC CASCADE");
        jdbc.execute("CREATE TABLE person (id INT, name VARCHAR(255))");
        jdbc.execute("INSERT INTO person values (1, 'John')");

        PersonDao dao = ctx.getBean(PersonDao.class);

        System.out.println(dao.getPersonName(1));
        System.out.println(dao.findAllPersons());
        System.out.println(dao.findAllPersonsAutoMapper());

        ((ConfigurableApplicationContext) ctx).close();
    }
}