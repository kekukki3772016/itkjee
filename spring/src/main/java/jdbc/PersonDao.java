package jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class PersonDao {

    @Resource
    public JdbcTemplate template;

    @Resource(name = "oracleDataSource")
    public DataSource oracleDs;

    @Resource(name = "hsqlDataSource")
    public DataSource hsqlDs;

    public String getPersonName(Integer id) {
        return template.queryForObject(
                "select name from person where id = ?", new Object[] { id },
                String.class);
    }

    public List<Person> findAllPersons() {
        return template.query("select * from person", new PersonMapper());
    }

    public List<Person> findAllPersonsAutoMapper() {
        return template.query("select * from person",
                new BeanPropertyRowMapper<Person>(Person.class));
    }

    private static class PersonMapper implements RowMapper<Person> {
        public Person mapRow(ResultSet rs, int rowNum) throws SQLException {

            Person person = new Person();

            person.setId(Integer.parseInt(rs.getString("id")));
            person.setName(rs.getString("name"));

            return person;
        }
    }

}
