DROP SCHEMA public CASCADE;

CREATE SEQUENCE seq1 START WITH 1;

CREATE TABLE PERSON (
       id BIGINT NOT NULL PRIMARY KEY,
       first_name VARCHAR(255) NOT NULL,
       name VARCHAR(255) NOT NULL,
       age_group_id INT NOT NULL
);

insert into person (id, first_name, name, age_group_id)
  values (next value for seq1, 'Jane', 'Doe', 1);

insert into person (id, first_name, name, age_group_id)
  values (next value for seq1, 'John', 'Doe', 2);