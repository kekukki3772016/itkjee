package mvc.controller;

import java.util.List;

import javax.annotation.Resource;

import mvc.dao.JpaPersonDao;
import mvc.model.Person;
import mvc.view.PersonForm;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

@Controller
public class PersonController {

    @Resource
    private JpaPersonDao jpaPersonDao;

    @ModelAttribute("personForm")
    public PersonForm getUserObject() {
        return new PersonForm();
    }

    @RequestMapping("/")
    public String home() {
        return "redirect:/list";
    }

    @RequestMapping("/list")
    public String personList(ModelMap model) {
        List<Person> persons = jpaPersonDao.findAll();

        model.addAttribute("persons", persons);

        return "list";
    }

    @RequestMapping("/add")
    public String showForm(@ModelAttribute("personForm") PersonForm form) {

        form.setAgeGroups(jpaPersonDao.getAgeGroups());

        return "form";
    }

    @RequestMapping("/delete/{personId}")
    public String deletePerson(@PathVariable("personId") Long personId) {
        jpaPersonDao.delete(personId);

        return "redirect:/list";
    }

    @RequestMapping("/edit")
    public String editPerson(@RequestParam("id") Long personId,
            @ModelAttribute("personForm") PersonForm form) {

        form.setPerson(jpaPersonDao.findById(personId));
        form.setAgeGroups(jpaPersonDao.getAgeGroups());

        return "form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveForm(
            @ModelAttribute("personForm") PersonForm form,
            BindingResult result,
            ModelMap model) {

        form.setAgeGroups(jpaPersonDao.getAgeGroups());

        ValidationUtils.rejectIfEmptyOrWhitespace(result, "person.firstName", "", "Sisesta eesnimi!");
        ValidationUtils.rejectIfEmptyOrWhitespace(result, "person.name", "", "Sisesta perekonnanimi!");
        if (result.hasErrors()) {
            return "form";
        }

        if (form.wasValidatePressed()) {
            form.setMessage("OK");
            return "form";
        } else {
            Person person = form.getPerson();
            jpaPersonDao.save(person);

            return "redirect:/view/" + person.getId();
        }
    }

    @RequestMapping("/view/{personId}")
    public String view(@PathVariable("personId") Long personId,
            @ModelAttribute("personForm") PersonForm form) {

        form.setPerson(jpaPersonDao.findById(personId));

        return "view";
    }
}