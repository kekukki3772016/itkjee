package mvc.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import service.ServiceException;

@Controller
@RequestMapping("/example")
public class ExampleController {

    @RequestMapping("/servlet")
    public String servlet(HttpServletRequest request, HttpServletResponse response) {

        System.out.println(request);
        System.out.println(response);

        return "message";

    }

    @RequestMapping("/exception")
    public String commonException() {

        throw new RuntimeException("some programming error");

    }

    @RequestMapping("/serviceException")
    public String serviceException() {

        throw new ServiceException("can't connect");
    }

    @RequestMapping("/counter")
    public String counter(HttpSession session) {

        Integer i = (Integer) session.getAttribute("counter");
        if (i == null) i = 0;

        session.setAttribute("counter", ++i);

        return "counter";
    }

    @RequestMapping("/date") // /date?date=2013-10-21
    public String bind(
            @RequestParam("date") @DateTimeFormat(iso = ISO.DATE) Date date,
            ModelMap model) {

        model.addAttribute("message", date);

        return "message";
    }
}