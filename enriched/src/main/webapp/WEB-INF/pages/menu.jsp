<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<ul id="menu">
    <li><a href="<c:url value='/info' />"><spring:message code="menu.info" /></a></li>
    <li><a href="<c:url value='/message' />"><spring:message code="menu.message" /></a></li>
    <li><a href="<c:url value='/logout' />"><spring:message code="menu.logout" /></a></li>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
      <li><a href="<c:url value='/admin/form' />"><spring:message code="menu.form" /></a></li>
    </sec:authorize>
</ul>

<sec:authorize access="isAuthenticated()">
  <spring:message code="label.user" />:
  <sec:authentication property="principal.username" />
</sec:authorize>

<jsp:include page="lang.jsp" /><br /><br />
