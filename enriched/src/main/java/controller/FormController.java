package controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import view.PersonForm;

@Controller
@RequestMapping("/admin")
public class FormController {

    @RequestMapping("/form")
    public String showForm(@ModelAttribute PersonForm form) {
        return "form";
    }

    @RequestMapping(value="/form", method = RequestMethod.POST)
    public String saveForm(ModelMap model,
            @ModelAttribute @Valid PersonForm form, BindingResult result) {

        if (!result.hasErrors()) {
            model.addAttribute("message", "message.ok");
        }

        return "form";
    }
}
